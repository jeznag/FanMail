function doGet() {
  setup();
  
  ScriptApp.newTrigger("main")
   .timeBased()
   .everyMinutes(5) 
   .create();
  
  return HtmlService.createTemplateFromFile('index')
    .evaluate()
    .setSandboxMode(HtmlService.SandboxMode.NATIVE);
}

function setup() {
  var spreadsheet = getSpreadsheetFile();
  
  createSettingsSheet(spreadsheet);
  createStatsSheet(spreadsheet);
  createRawMailSheet(spreadsheet);
  createErrorLoggingSheet(spreadsheet);
 }

function getSpreadsheetFile() {
  var files = DriveApp.getFilesByName('FanMail Stats'),
      spreadsheet;
  
  while (files.hasNext()) {
    spreadsheet = SpreadsheetApp.open(files.next());
    SpreadsheetApp.setActiveSpreadsheet(spreadsheet);
  }
  
  if (!spreadsheet) {
    spreadsheet = SpreadsheetApp.create('FanMail Stats'); 
  } 
  
  return spreadsheet;
}

function createSettingsSheet(spreadsheet) {
  
  var settingsSheet = spreadsheet.getSheetByName("FanMail Settings");
  
  if(settingsSheet === null) {  //create the spreadsheet if not already created
    settingsSheet = spreadsheet.insertSheet("FanMail Settings", 0)
  }
  
  //creating the header row
  settingsSheet.getRange("A1").setValue("My Email Addresses");
  settingsSheet.getRange("B1").setValue("Senders I care about a lot");
  settingsSheet.getRange("C1").setValue("Senders I want to ignore");
  
  settingsSheet.getRange("A1:C1").setFontWeight("bold");
  //add default ignore settings
  settingsSheet.getRange("C2").setValue("apps-scripts-notifications@google.com");
  settingsSheet.getRange("C3").setValue("_REGEX_info@(.)*");
  settingsSheet.getRange("C4").setValue("_REGEX_mail@(.)*");
  settingsSheet.getRange("C5").setValue("_REGEX_marketing@(.)*");
  settingsSheet.getRange("C6").setValue("_REGEX_noreply@(.)*");
  settingsSheet.getRange("C7").setValue("_REGEX_no-reply@(.)*");
  settingsSheet.getRange("C8").setValue("_REGEX_donotreply@(.)*");
  settingsSheet.getRange("C9").setValue("_REGEX_notify@(.)*");
  settingsSheet.getRange("C10").setValue("_REGEX_mailbot@(.)*");
  settingsSheet.getRange("C11").setValue("_REGEX_news@(.)*"); 
}

function createRawMailSheet(spreadsheet) {
  var sheet = spreadsheet.getSheetByName("FanMail Raw Data");
  
  if(sheet === null)  //create the spreadsheet if not already created
  {
    sheet = spreadsheet.insertSheet("FanMail Raw Data", 2)
  }
  //creating the header row
  sheet.getRange("A1").setValue("Sender");
  sheet.getRange("B1").setValue("Subject");
  sheet.getRange("C1").setValue("Raw Message");
  sheet.getRange("D1").setValue("Message without signature");
  sheet.getRange("E1").setValue("Sentiment score");
  sheet.getRange("F1").setValue("Time of message");
  sheet.getRange("G1").setValue("D score");
  sheet.getRange("H1").setValue("I score");
  sheet.getRange("I1").setValue("S score");
  sheet.getRange("J1").setValue("C score");
  sheet.getRange("K1").setValue("Message ID");
}

function createStatsSheet(spreadsheet) {
 
  var sheet = spreadsheet.getSheetByName("FanMail Stats");
  
  if(sheet === null)  //create the spreadsheet if not already created
  {
    sheet = spreadsheet.insertSheet("FanMail Stats", 0)
  }
  
  //creating the header row
  sheet.getRange("A1").setValue("Sender");
  sheet.getRange("B1").setValue("Total Messages Sent");
  sheet.getRange("C1").setValue("Total Sentiment score");
  sheet.getRange("D1").setValue("Average Sentiment score per message"); 
  sheet.getRange("E1").setValue("D score");
  sheet.getRange("F1").setValue("I score");
  sheet.getRange("G1").setValue("S score");
  sheet.getRange("H1").setValue("C score");
  
  sheet.getRange("A1:E1").setFontWeight("bold"); 
}

function createErrorLoggingSheet(spreadsheet) {
 
  var sheet = spreadsheet.getSheetByName("FanMail Errors");
  
  if(sheet === null)  //create the spreadsheet if not already created
  {
    sheet = spreadsheet.insertSheet("FanMail Errors", 3)
  }
  
  //creating the header row
  sheet.getRange("A1").setValue("Error");
  sheet.getRange("B1").setValue("Stack Trace");
  
  sheet.getRange("A1:E1").setFontWeight("bold"); 
}
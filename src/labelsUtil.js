
function updateLabelForThread(thread, averageSentimentScoreForThisThread) {
  var labelToApply = getLabelToApply(averageSentimentScoreForThisThread); 
  
  if (labelToApply) {
    labelToApply.addToThread(thread);
  } 
  
  processedLabel.addToThread(thread);
}

function getLabelToApply(averageSentimentScore) {
  
  if (averageSentimentScore < 0 && averageSentimentScore > -3) {
  	return slightlyAnnoyedLabel;
  } 
  else if (averageSentimentScore <= -3 && averageSentimentScore > -6) {
  	return prettyAngryLabel;
  }
  else if (averageSentimentScore <= -6) {
  	return reallyAngryLabel;
  }
  else if (averageSentimentScore > 0 && averageSentimentScore < 3) {
  	return slightlyHappyLabel;
  } 
  else if (averageSentimentScore >= 3 && averageSentimentScore < 6) {
  	return prettyHappyLabel;
  }
  else if (averageSentimentScore >= 6 && averageSentimentScore < 15) {
  	return cheerfulLabel;
  } 
  else if (averageSentimentScore >= 15) {
  	return delightedLabel;
  } 
  else if (averageSentimentScore === NaN || averageSentimentScore === 0) {
    return neutralLabel; 
  }
  else {
     Logger.log("Couldn't find matching label for " + averageSentimentScore);
  }
}
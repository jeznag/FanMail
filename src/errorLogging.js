function logErrorInSheet(e) {
  Logger.log(e); Logger.log(e.stack);
  
  var ss = SpreadsheetApp.getActiveSpreadsheet();
  var sheet = ss.getSheetByName("FanMail Errors");
  sheet.appendRow([e, e.stack]);
}

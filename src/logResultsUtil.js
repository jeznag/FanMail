
function updateSentimentScores(sender, score, discProfileResults, message, rawMessage, subject, messageID) {
  
  var rowForSender = getRowForSender(sender);
  var ss = SpreadsheetApp.getActiveSpreadsheet();
  var sheet = ss.getSheetByName("FanMail Stats");
  
  if (!sender || sender.length === 0) {
    return; 
  }
  
  if (!rowForSender) {
    sheet.appendRow([sender, 1, score, score, discProfileResults.D, discProfileResults.I, discProfileResults.S, discProfileResults.C]);
    return; 
  }
  
  sheet.getRange("A" + rowForSender).setValue(sender); 
  var currentTotalMessages = logTotalCurrentMessages(sheet); 
  var currentTotalSentiment = logTotalCurrentSentiment(sheet); 
  var averageSentiment = logAverageSentiment(sheet, currentTotalMessages, currentTotalSentiment);
  
  setCellColourBasedOnSentiment(sheet, averageSentiment); 
  addRawData(sender, score, discProfileResults, message, rawMessage, subject, messageID);
  addDISCProfileResults(discProfileResults, sheet, rowForSender);
}

function setCellColourBasedOnSentiment (sheet, averageSentiment) {
  if (averageSentiment < 0) {
     sheet.getRange("D" + rowForSender).setBackgroundColor('red');
  }
  else if (averageSentiment < 10) {
     sheet.getRange("D" + rowForSender).setBackgroundColor('orange');
  } 
  else if (averageSentiment >= 10) {
     sheet.getRange("D" + rowForSender).setBackgroundColor('lightgreen');
  } 
}

function logAverageSentiment (sheet, currentTotalMessages, currentTotalSentiment) {
  var averageSentiment = currentTotalSentiment / currentTotalMessages;
  
  sheet.getRange("D" + rowForSender).setValue(averageSentiment); 
  
  return averageSentiment;
}

function logTotalCurrentSentiment(sheet) {
  var currentTotalSentiment;
  sheet.getRange("C" + rowForSender).getValue();
  if (!currentTotalSentiment || currentTotalSentiment === '') {
    currentTotalSentiment = 0;
  }
  
  currentTotalSentiment = parseInt(currentTotalSentiment);
  
  currentTotalSentiment += score;
  sheet.getRange("C" + rowForSender).setValue(currentTotalSentiment);
  
  return currentTotalSentiment;
}

function logTotalCurrentMessages (sheet) {
  var currentTotalMessages;
  sheet.getRange("B" + rowForSender).getValue();
  if (!currentTotalMessages || currentTotalMessages === '') {
    currentTotalMessages = 0;
  }
  
  currentTotalMessages += 1;
  sheet.getRange("B" + rowForSender).setValue(currentTotalMessages); 
  return currentTotalMessages;
}

function addDISCProfileResults(discProfileResults, sheet, rowForSender) {
  try {
    
    var currentDValue = parseInt(sheet.getRange("E" + rowForSender).getValue()) || 0;
    var currentIValue = parseInt(sheet.getRange("F" + rowForSender).getValue()) || 0;
    var currentSValue = parseInt(sheet.getRange("G" + rowForSender).getValue()) || 0;
    var currentCValue = parseInt(sheet.getRange("H" + rowForSender).getValue()) || 0;
    
    setValueIfNumeric(sheet, "E" + rowForSender, currentDValue + discProfileResults.D);
    
    setValueIfNumeric(sheet, "F" + rowForSender, currentIValue + discProfileResults.I);
    
    setValueIfNumeric(sheet, "G" + rowForSender, currentSValue + discProfileResults.S);
    
    setValueIfNumeric(sheet, "H" + rowForSender, currentCValue + discProfileResults.C);
    
  } catch (e) {
     Logger.log('issue with setting disc profile results' + rowForSender + e + e.stack); 
  }
}

function addRawData(sender, score, discProfileResults, message, rawMessage, subject, messageID) {
  var ss = SpreadsheetApp.getActiveSpreadsheet();
  var sheet = ss.getSheetByName("FanMail Raw Data");
  Logger.log('adding raw data' + 'subject: ' + subject + '\nmessage: ' + message + '\nraw message:' + rawMessage + 'messageID' + messageID);
  var rowForSender = sheet.appendRow([sender, subject || 'unknown', rawMessage, message, score, (new Date()).toString(), discProfileResults.D, discProfileResults.I, discProfileResults.S, discProfileResults.C, messageID || 'unknown ID']);
}

function setValueIfNumeric(sheet, rangeString, newValue) {
  if (IsNumeric(newValue)) { 
    sheet.getRange(rangeString).setValue(currentDValue + discProfileResults.D);
  }
}

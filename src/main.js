function main() {
  
  setup();
  
  var files = DriveApp.getFilesByName('FanMail Stats'),
      spreadsheet;
  
  while (files.hasNext()) {
    spreadsheet = SpreadsheetApp.open(files.next());
    SpreadsheetApp.setActiveSpreadsheet(spreadsheet);
  }
  
  var ss = SpreadsheetApp.getActiveSpreadsheet();
  var sheet = ss.getSheetByName("FanMail Stats");
  
  createLabels();
  
  //only search for emails received in the last two weeks that haven't been processed
  var currentTime = new Date();
  currentTime.setDate(currentTime.getDate()-14);
  var twoWeeksAgoDateString = currentTime.getFullYear() + "/" + (currentTime.getMonth() + 1) + "/" + currentTime.getDate();  
  
  var query1 = "NOT(label:fanmail-processed) AND after:" + twoWeeksAgoDateString;
  var threads1 = GmailApp.search(query1);
  try {
    processThreads(threads1);
  } catch (e) {
    logErrorInSheet(e);
  }
}

function isTimeUp_(start) {
  var now = new Date();
  return now.getTime() - start.getTime() > 300000; // 5 minutes
}

function processThreads(threads1) {
  var start = new Date();
  for each (thread in threads1)
  {
      if (isTimeUp_(start)) {
        Logger.log("Time up");
        break;
      }
      processIndividualThread(thread);
  }
}

function processIndividualThread(thread) {
  var messages = thread.getMessages(),
      totalReceivedMessages = 0,
      totalSentimentScoreInThisThread = 0,
      averageSentimentScoreInThisThread = 0;
  
  if (!messages || !messages.length) {
    return;
  }
  
  for (var i = 0; i < messages.length; i++) {
    var messageID = messages[i].getId();
    var sender = messages[i].getFrom()
    if (!shouldIgnoreMessage(sender, messageID)) {
      try {
        var plainMessage;
        try {
          plainMessage = messages[i].getPlainBody();
        } catch (e) {
          logErrorInSheet(e);
          
          plainMessage = getPlainText(messages[i].getBody());
        }
        Logger.log(plainMessage);
        var message = emailParseUtil.removeQuotedTextFromEmail(plainMessage);
        var sentimentScore = analyseSentiment(message).score,
            readabilityScoreForThisMessage = calculateReadabilityScore(message),
            resultsFromDISC = discProfileAnalyser.analyseEmail(message, readabilityScoreForThisMessage, sentimentScore);
        
        if (sentimentScore < 0) {
          thread.markImportant(); 
        }
        
        updateSentimentScores(sender, sentimentScore, resultsFromDISC, message, plainMessage, messages[i].getSubject(), messages[i].getId());
        totalReceivedMessages++;
        totalSentimentScoreInThisThread += sentimentScore;
        
      }
      catch (e) {
        logErrorInSheet(e);
      }
    }
    else {
      Logger.log("***Ignoring message cos it's on the ignore list: " + messages[i].getFrom() + " to " + messages[i].getTo());
      totalReceivedMessages++;
    }
  }
         
  if (totalReceivedMessages > 0) {
    averageSentimentScoreInThisThread = totalSentimentScoreInThisThread / totalReceivedMessages;
    updateLabelForThread(thread, averageSentimentScoreInThisThread, processedLabel);
  } 
}

function IsNumeric(s) {
  return !isNaN(parseFloat(s)) && isFinite(s);
}

function getRowForSender(senderName) {
  
  var ss = SpreadsheetApp.getActiveSpreadsheet();
  var sheet = ss.getSheetByName("FanMail Stats");
  
  var range = sheet.getRange(2, 1, 2000);
  var values = range.getValues();
  var correctRow;
  
  for (var row in values) {
    for (var col in values[row]) {
      var senderInThisCell = values[row][col];
      if(senderInThisCell === senderName) {
         Logger.log("Found " + senderName + " in row " + row);
         return parseInt(row) + 2;
      }  
    }
  } 
  
  return false;
}

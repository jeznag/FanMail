var processedLabel,
    slightlyAnnoyedLabel,
    prettyAngryLabel,
    reallyAngryLabel,
    slightlyHappyLabel,
    prettyHappyLabel,
    cheerfulLabel,
    delightedLabel,
    neutralLabel;

function createLabels() {
  processedLabel = createLabelIfDoesNotExist("FanMail-processed");
  slightlyAnnoyedLabel = createLabelIfDoesNotExist("FanMail-slightlyAnnoyed");
  prettyAngryLabel = createLabelIfDoesNotExist("FanMail-prettyAngry");
  reallyAngryLabel = createLabelIfDoesNotExist("FanMail-reallyAngry");
  slightlyHappyLabel = createLabelIfDoesNotExist("FanMail-slightlyHappy");
  prettyHappyLabel = createLabelIfDoesNotExist("FanMail-prettyHappy");
  cheerfulLabel = createLabelIfDoesNotExist("FanMail-cheerful");
  delightedLabel = createLabelIfDoesNotExist("FanMail-delighted");
  neutralLabel = createLabelIfDoesNotExist("FanMail-neutral"); 
}

function createLabelIfDoesNotExist(labelName) {
  var label = GmailApp.getUserLabelByName(labelName); 
  if (!label) {
    label = GmailApp.createLabel(labelName);
  }
  
  return label;
}
var emailaddressesToIgnore;
var emailAddressesWeOwn;
var emailAddressesWeShouldIgnore;
var messageIDsAlreadyProcessed;

function shouldIgnoreMessage(sender, messageID) {
  var addressesToIgnore = getEmailAddressesWeOwn().concat(getEmailAddressesWeShouldIgnore()),
      recipientMatchesOurList = false;
  
  if (messageIDsAlreadyProcessed && messageIDsAlreadyProcessed.indexOf(messageID) > -1) {
    return true; 
  }
  
  addressesToIgnore.some(function(address) { 
    if (address.indexOf("_REGEX_") > -1) {
      var regexMatcher = new RegExp(address.split("_REGEX_")[1]);
      if (regexMatcher.test(sender)) {
        recipientMatchesOurList = true;
        return true;
      }
    } else if (address.indexOf("_REGEX_") === -1) {
      if (sender.indexOf(address) > - 1) {
        recipientMatchesOurList = true;
        return true; 
      }
    }
  });
  
  return recipientMatchesOurList;
}

function getMessageIDsAlreadyProcessed() {
  if (messageIDsAlreadyProcessed) {
    return messageIDsAlreadyProcessed; 
  }
  
  try {
  
    messageIDsAlreadyProcessed = [];
    
    var ss = SpreadsheetApp.getActiveSpreadsheet();
    var sheet = ss.getSheetByName("FanMail Raw Data");
    
    var range = sheet.getRange(2, 11, 10000);
    var values = range.getValues();
    var correctRow;
    
    for (var row in values) {
      for (var col in values[row]) {
        var messageID = values[row][col];
        if (messageID && messageID.length > 0) {
          messageIDsAlreadyProcessed.push(messageID);
        }
      }
    } 
    
    messageIDsAlreadyProcessed.push(Session.getActiveUser().getEmail());
    
  } catch (e) {
    Logger.log('error with messageIDsAlreadyProcessed' + e + e.stack); 
  }
  
  return messageIDsAlreadyProcessed;
}

function getEmailAddressesWeOwn() {
  if (emailAddressesWeOwn) {
    return emailAddressesWeOwn; 
  }
  
  try {
  
    emailAddressesWeOwn = [];
    
    var ss = SpreadsheetApp.getActiveSpreadsheet();
    var sheet = ss.getSheetByName("FanMail Settings");
    
    var range = sheet.getRange(2, 1, 1000);
    var values = range.getValues();
    var correctRow;
    
    for (var row in values) {
      for (var col in values[row]) {
        var emailAddress = values[row][col];
        if (emailAddress && emailAddress.length > 0) {
          emailAddressesWeOwn.push(values[row][col]);
        }
      }
    } 
    
    emailAddressesWeOwn.push(Session.getActiveUser().getEmail());
    
  } catch (e) {
    Logger.log('error with getEmailAddressesWeOwn' + e + e.stack); 
  }
  
  return emailAddressesWeOwn;
}

function getEmailAddressesWeShouldIgnore() {
  if (emailAddressesWeShouldIgnore) {
    return emailAddressesWeShouldIgnore; 
  }
  
  emailAddressesWeShouldIgnore = [];
  
  var ss = SpreadsheetApp.getActiveSpreadsheet();
  var sheet = ss.getSheetByName("FanMail Settings");
  
  var range = sheet.getRange(2, 3, 1000);
  var values = range.getValues();
  var correctRow;
  
  for (var row in values) {
    for (var col in values[row]) {
      var emailAddress = values[row][col];
      if (emailAddress && emailAddress.length > 0) {
        emailAddressesWeShouldIgnore.push(values[row][col]);
      }
    }
  } 
  
  Logger.log("ignore these addresses: " + emailAddressesWeShouldIgnore);
  
  return emailAddressesWeShouldIgnore;
}
function getPlainText(html) {
  var div = document.createElement("div");
  div.innerHTML = html;
  var text = div.innerText;
  return text;
}
